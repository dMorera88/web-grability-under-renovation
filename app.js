var form = document.getElementById("contact-form");
var successMessage = document.getElementById("successMessage"); 
var errorMessage = document.getElementById("errorMessage");
var organization;  
var email;
var message;  
var userName;
var cleanForm = function () {
	organization.value = '';  
	email.value = '';
	message.value = '';
	userName.value = '';
};

form.addEventListener("submit", function(event) {

    organization = document.getElementById("organization");
	email = document.getElementById("email");
	message = document.getElementById("message");
 	userName = document.getElementById("userName");
 	
    var http = new XMLHttpRequest(),
	    params =  {
	    	name: userName.value,
	    	organization: organization.value,
	    	email: email.value,
	    	message: message.value
	    };

	successMessage.classList.add("hiddenMessage");
    errorMessage.classList.add("hiddenMessage");
     
	http.open("POST", 'https://u6gib8vmj9.execute-api.us-east-1.amazonaws.com/pro/contact-form', true);

	http.onreadystatechange = function() {
		
	    if(http.status !== 200) {
	    	successMessage.classList.add("hiddenMessage");
    		errorMessage.classList.add("error");
    		errorMessage.classList.remove("hiddenMessage");
    		
	    } else {
	    	
	       	successMessage.classList.add("success");
    		errorMessage.classList.add("hiddenMessage");
    		successMessage.classList.remove("hiddenMessage");
    		cleanForm();
    		setTimeout(function(){
    			successMessage.classList.add("hiddenMessage");
    		}, 4000)

	    }
	}

	http.send(JSON.stringify(params));
	event.preventDefault();
});	

